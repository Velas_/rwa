Cypress.Commands.add('goToApp', (url) => {
    cy.visit(url);
});

Cypress.Commands.add('closebrowser', () => {
    //Closing the Browser through a Cypress comand is not supported.
});

Cypress.Commands.add('setUserName', (userName) => {
    cy.get('#username').type('{selectAll}').type(userName);
});

Cypress.Commands.add('setPassword', (password) => {
    cy.get('#password').type('{selectAll}').type(password);
});

Cypress.Commands.add('clickRememberMe', () => {
    cy.get('input[name="remember"]').check();
});

Cypress.Commands.add('clickSignIn', () => {
    cy.get('[data-test="signin-submit"]').click();
});

Cypress.Commands.add('checkConnection', () => {
    cy.get('[data-test="main"]').should('be.visible');
});

Cypress.Commands.add('checkNotConnection', () => {
    cy.get('[data-test="main"]').should('not be.visible');
});

Cypress.Commands.add('checkTransactionSubmitted', () => {
    cy.get('.MuiAlert-message').should('be.visible');
});

Cypress.Commands.add('clickSignUp', () => {
    cy.get('[data-test="signup-submit"]').click();
});

Cypress.Commands.add('checkAccountNotCreated', () => {
    cy.get('#confirmPassword-helper-text').should('be.visible');
});

Cypress.Commands.add('createAccount', () => {
    cy.get('[data-test="signup"]').click();
});

Cypress.Commands.add('haveAlreadyAnAccount', () => {
    cy.get('a[href*="/signin"]').click();
});

Cypress.Commands.add('enterFirstName', (firstName) => {
    cy.get('#firstName').type('{selectAll}').type(firstName);
});

Cypress.Commands.add('enterLastName', (lastName) => {
    cy.get('#lastName').type('{selectAll}').type(lastName);
});

Cypress.Commands.add('enterUserName', (userName) => {
    cy.get('#username').type('{selectAll}').type(userName);
});

Cypress.Commands.add('enterPassword', (pwd) => {
    cy.get('#password').type('{selectAll}').type(pwd);
});

Cypress.Commands.add('enterconfirmPassword', (confirmPwd) => {
    cy.get('[data-test="signup-confirmPassword"]').type('{selectAll}').type(confirmPwd);
});

/* connection has been made */

Cypress.Commands.add('modifyDate', () => {
    cy.get('[data-test="transaction-list-filter-date-range-button"]').click();
});

Cypress.Commands.add('clickOnCalendar', (date) => {
    cy.get('[data-date="' + date + '"]').click();
});

Cypress.Commands.add('clearDate', () => {
    cy.get('[data-test="transaction-list-filter-date-clear-button"]').click();
});

Cypress.Commands.add('clickContactTab', () => {
    cy.get('[data-test="nav-contacts-tab"]').click();
});

Cypress.Commands.add('clickPersonalTab', () => {
    cy.get('[data-test="nav-personal-tab"]').click();
});

Cypress.Commands.add('checkPersonalTab', () => {
    cy.contains('personal');
});

Cypress.Commands.add('clickPublicTab', () => {
    cy.get('[data-test="nav-public-tab"]').click();
});

Cypress.Commands.add('clickOnTransaction', () => {
    cy.scrollTo('top');
    cy.get('[data-test="transaction-list"] li').first().click();
});

Cypress.Commands.add('clickLikeTransaction', () => {
    cy.get('[data-test="transaction-like-button-' + /.*/ + '"]').click();
});

Cypress.Commands.add('enterCommentTransaction', (comment) => {
    cy.get('[data-test="transaction-comment-input-' + /.*/ + '"]').type('{selectAll}').type(comment+'{enter}');
});

Cypress.Commands.add('checkNewComment', () => {
    cy.get('[data-test="comments-list"] li').first().should('have.length', 1);
});

Cypress.Commands.add('clickHomeTab', () => {
    cy.get('[data-test="sidenav-home"]').click();
});

/* payment part */

Cypress.Commands.add('createNewTransaction', () => {
    cy.get('[data-test="nav-top-new-transaction"]').click();
});

Cypress.Commands.add('selectTransactionContact', () => {
    cy.get('[data-test="users-list"]:first').click();
});

Cypress.Commands.add('enterTransactionAmount', (amount) => {
    cy.get('[data-test="transaction-create-amount-input"]').type('{selectAll}').type(amount);
});

Cypress.Commands.add('enterTransactionNote', (note) => {
    cy.get('[data-test="transaction-create-description-input"]').type('{selectAll}').type(note);
});

Cypress.Commands.add('clickTransactionRequest', () => {
    cy.get('[data-test="transaction-create-submit-request"]').click();
});

Cypress.Commands.add('clickTransactionPay', () => {
    cy.get('[data-test="transaction-create-submit-payment"]').click();
});

Cypress.Commands.add('clickReturnToTransaction', () => {
    cy.get('[data-test="new-transaction-return-to-transaction"]').click();
});

Cypress.Commands.add('clickCreateOtherTransaction', () => {
    cy.get('[data-test="new-transaction-create-another-transaction"]').click();
});
